using Test
using TestItemRunner

@testitem "Unit slicer" tags=[:unit, :unitslice] begin
	include("UnitSlicer.jl")
end

@run_package_tests filter=ti->(:unit in ti.tags)