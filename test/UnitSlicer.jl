using Test

using SliceSamplers: unitslice, samples
using UniformSamplers: uniform
using LinearAlgebra: svd
using Statistics: mean

using Random

# initialize random values
Random.seed!(3577902255811995474)

# n - number of dimensions of ambient space
# m - number of dimensions of slice
function test_unitslicer(n, m)

	# generate sampler
	sampler = unitslice(uniform(100, m), rand(n, n-m))

	# each sample of slice should still be bound by original limits
	@test all(map(sampler) do sample
		all(xᵢ -> 0 ≤ xᵢ ≤ 1, sample)
	end)

	# fetch samples directly
	x = samples(sampler)

	# compute mean of all samples
	x̄ = mean(x, dims=2)

	# compute singular values of mean shifted samples
	_, S, _ = svd(x.-x̄)

	# sum of singular values of m first values should contain all information -> no variance leaks outside the slice
	@test sum(Iterators.take(S, m))/sum(S) ≈ 1

	# Test number of samples
	@test length(sampler) == 100

	# Test number of dimensions
	@test ndims(sampler) == n

	# Due to transformation of slicer, these are only numerically equal
	@test sampler[1] ≈ samples(sampler)[:,1]

	nothing
end

test_unitslicer(3, 2)

test_unitslicer(10, 4)