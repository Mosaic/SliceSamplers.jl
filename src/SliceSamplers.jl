# Copyright (C) 2024 Dominik Itner
# SliceSamplers.jl is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# SliceSamplers.jl is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License along with SliceSamplers.jl.  If not, see <http://www.gnu Lesser.org/licenses/>.
# A copy of the GNU Lesser General Public License has deliberately not been included to circumvent unnecessary traffic due to the high degree of modularity and interoperability of packages in Mosaic.jl.
# The license can instead be found at <https://codeberg.org/Mosaic/License>.

module SliceSamplers

# External dependencies

using LinearAlgebra
using Nullspaces

using SamplerBase.Core: Sampler
using SamplerBase.Interface: samples

import SamplerBase.Interface as SamplerInterface

# Internal dependencies

include("Slicer.jl")

# Extend core types

"A sampler that slices a higher dimensional sample space to produce a lower dimensional sampling space."
struct SliceSampler{C,S} <: Sampler
	slicer::C
	subsampler::S
end

## Unit slice sampler

### Alias

const UnitSliceSampler{S} = SliceSampler{UnitSlicer,S} where S

### Constructor

"""
	unitslice(subsampler::Sampler, N::AbstractMatrix, p::AbstractVector)

Sample an `n-m`-dimensional slice of an `n`-dimensional hypercube given `m` normal vectors provided in a matrix `N ∈ ℝⁿˣᵐ` that span the nullspace of the slice.
The slice is automatically fit within the bounds `[0, 1]ⁿ` such that the square itself is symmetric, i.e., a two-dimensional slice is a square, a three-dimensional slice is a cube.

*No* checks are currently performed to asure the dimensionality of the provided `subsampler` and normal vectors agree!
"""
function unitslice(subsampler, N::AbstractMatrix, p=zeros(size(N, 2)))
	# create slicer
	slicer = UnitSlicer(N, p)
	# assemble slice sampler
	SliceSampler(slicer, subsampler)
end

# Interface

## Extend interface

## SamplerBase: length

# Length is provided by subsampler
SamplerInterface.length(sampler::SliceSampler) =
	length(sampler.subsampler)

## SamplerBase: ndims

# Number of dimensions conforms to ambient space and is defined in slicer
SamplerInterface.ndims(sampler::SliceSampler) =
	length(position(sampler.slicer))

## SamplerBase: getindex

# Access samples and transform them according to slice
SamplerInterface.getindex(sampler::SliceSampler, i...) =
	sampler.slicer * sampler.subsampler[i...]

## SamplerBase: samples

# Fetch all samples and transform them according to slice
SamplerInterface.samples(sampler::SliceSampler) =
	sampler.slicer * samples(sampler.subsampler)

end # SliceSamplers