# Super type

abstract type AbstractSlicer end

# Extend interface

function Base.:*(slice::AbstractSlicer, x::U) where U<:Union{AbstractVector,AbstractMatrix}
	# project samples x into basis e
	y = slice.e'x
	# add position of slice to projection
	broadcast!(+, y, slice.p, y)
end

# Internal interface

position(slice::AbstractSlicer) =
	slice.p

# Implementation

## Unit slice

struct UnitSlicer{P<:AbstractVector,B<:AbstractMatrix} <: AbstractSlicer
	p::P
	e::B
end

### Constructor

function UnitSlicer(N::U, p=zeros(size(N, 2))) where U<:Union{AbstractVector,AbstractMatrix}
	# conclude dimensionality of ambient space
	n = first(size(N))
	# compute center of slice
	c = fill(0.5, n) + N*p
	# find basis vectors of slice (transposed: Ē = Eᵀ)
	Ē = leftnullspace(N')
	# conclude dimensionality of slice space
	m = size(Ē, 1)
	# compute permutation of corners of slice space
	ρ = permutecorners(m)
	# find smallest intersection with n-cube
	α = findminintersection(Ē'ρ, c)
	# return slice
	#   - compute position vector in all negative quadrant as origin
	#   - scale basis by 2α so that (1,1) maps to opposite corner
	#   - this guarantees a unit subcube
	UnitSlicer(c + Ē'fill(-α, m), 2α*Ē)
end

#### Dependencies for constructor

"""
	findminintersection(V, [c])

Given a vector `v ∈ V` or a set of vectors `vᵢ ∈ V`, find rescaled `vᵢ` such that they intersect with the unit `n`-cube, assuming `vᵢ` are centered at `c ∈ ℝⁿ`.
Default is `c = [0.5, ...]ᵀ` at the center of the unit `n`-cube.
"""
function findminintersection(V::U, c=fill(0.5, first(size(V)))) where U<:Union{AbstractVector,AbstractMatrix}
	# initialize scales
	α = similar(V, size(V, 2))
	# iterate over all vectors
	for (k, vₖ) in enumerate(eachcol(V))
		# iterate over elements of current vector and center
		for (cᵢ, vᵢ) in zip(c, vₖ)
			# compute scale factor
			αᵢ = min(cᵢ, 1-cᵢ) / vᵢ
			# if all rescaled elements fit in n-cube, it is correct
			if all(vᵢ -> 0 ≤ (cᵢ + αᵢ * vᵢ) ≤ 1, vₖ)
				# save scale
				α[k] = αᵢ
				# break loop early
				break
			end
		end
	end
	# done
	first(findmin(abs, α))
end

"""
	permutecorners(n, v)

Find all `mⁿ` corners in `n` dimensions which are a permutation of coordinates in `v ∈ ℝᵐ`.
"""
function permutecorners(n::Int, v::AbstractVector=[-1, +1])
	# initialize permutation
	ρ = similar(v, n, first(size(v))^n)
	# iterate over perumuations
	for (i, ρᵢ) in enumerate(Iterators.product(fill(v, n)...))
		ρ[:,i] = [ρᵢ...]
	end
	# done
	ρ
end